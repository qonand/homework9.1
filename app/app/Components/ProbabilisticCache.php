<?php

namespace App\Components;

use Illuminate\Redis\Connections\PredisClusterConnection;
use Psr\SimpleCache\CacheInterface;
use Psr\SimpleCache\InvalidArgumentException;

class ProbabilisticCache
{
    /**
     * @var PredisClusterConnection
     */
    private $connection;

    /**
     * @param PredisClusterConnection $connection
     */
    public function __construct(PredisClusterConnection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param string $key
     * @param int $ttl
     * @param callable $function
     * @return mixed|void
     * @throws InvalidArgumentException
     */
    public function getOrSet(string $key, int $ttl, callable $function)
    {
        $data = unserialize($this->connection->get($key));

        ['value' => $value, 'delta' => $delta] = $data;

        $remainTtl = $this->connection->ttl($key);
        echo 'Remain ttl: ', $remainTtl, PHP_EOL;

        $probability = abs(log(rand(0,100) / 100));
        if ($delta === null || $remainTtl - $delta * $probability <= 0) {
            echo "\e[31mCache recalculate", PHP_EOL;
            $start = time();
            $value = call_user_func($function);
            $delta = time() - $start;

            $this->connection->setex($key, $ttl, serialize(['value' => $value, 'delta' => $delta]));
        }

        return $value;
    }
}
