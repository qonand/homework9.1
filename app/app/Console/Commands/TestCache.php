<?php

namespace App\Console\Commands;

use App\Components\ProbabilisticCache;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;

class TestCache extends Command
{
    /**
     * @var ProbabilisticCache
     */
    private $cache;

    public function __construct()
    {
        parent::__construct();
        $this->cache = new ProbabilisticCache(Redis::connection('cache'));
    }

    /**
     * {@inheritdoc}
     */
    protected $signature = 'test-cache:run';

    /**
     * {@inheritdoc}
     */
    protected $description = 'Test probabilistic cache';

    /**
     * {@inheritdoc}
     */
    public function handle(): int
    {
        $data = $this->cache->getOrSet('test-cache', 10, static function() {
            sleep(3);
            return 'this is test probabilistic cache';
        });

        $this->info($data);

        return static::SUCCESS;
    }
}
