# How to install the application

1. Clone the repository `git clone https://qonand@bitbucket.org/qonand/homework9.1.git`
2. Run `docker-compose up -d nginx redis-cluster workspace` in `laradock` folder
3. Run `docker-compose exec workspace bash` in `laradock` folder and in opened bash run the following commands:

 - `composer install`
 - `php artisan key:generate`

# How to run the application
1. Run `docker-compose up -d nginx redis-cluster workspace` in `laradock` folder
2. Run `docker-compose exec workspace bash` in `laradock` folder to open bash
3. Run `artisan test-cache:run`

# Result
Result of probabilistic cache flushing implementation you can see here https://bitbucket.org/qonand/homework9.1/src/master/app/app/Components/ProbabilisticCache.php